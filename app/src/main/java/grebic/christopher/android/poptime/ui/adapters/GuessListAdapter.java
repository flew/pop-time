package grebic.christopher.android.poptime.ui.adapters;

import android.content.Context;
import android.support.design.widget.Snackbar;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.List;

import grebic.christopher.android.poptime.R;
import grebic.christopher.android.poptime.game.logic.Guess;

/**
 * A {@link RecyclerView} adapter for displaying {@link Guess}.
 *
 * @author Christopher Grébic
 */
public class GuessListAdapter extends RecyclerView.Adapter<GuessListAdapter.ViewHolder> {

    private Context context;
    private View main;
    private List<Guess> guesses;
    private Guess lastDeletedGuess;
    private int lastDeletedGuessIndex;

    public GuessListAdapter(Context context, View main, List<Guess> guesses) {
        this.context = context;
        this.main = main;
        this.guesses = guesses;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_guess, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final int reverseIndex = guesses.size() - holder.getAdapterPosition() - 1;
        final Guess guess = guesses.get(reverseIndex);
        holder.guessTextView.setText(guess.toString());

        final Snackbar undoSnackbar = Snackbar.make(main, R.string.deleted, Snackbar.LENGTH_SHORT);

        TextView snackbarText = undoSnackbar.getView().findViewById(android.support.design.R.id.snackbar_text);
        snackbarText.setTypeface(ResourcesCompat.getFont(context, R.font.baloo));

        TextView snackbarButton = undoSnackbar.getView().findViewById(android.support.design.R.id.snackbar_action);
        snackbarButton.setTypeface(ResourcesCompat.getFont(context, R.font.baloo));
        snackbarButton.setTextColor(context.getResources().getColor(R.color.colorAccentSecondary));

        undoSnackbar.setAction(R.string.undo, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (lastDeletedGuess != null) {
                    guesses.add(lastDeletedGuessIndex, lastDeletedGuess);
                    lastDeletedGuess = null;
                    notifyItemInserted(-lastDeletedGuessIndex + guesses.size() - 1);
                }
            }
        });

        holder.deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int reverseIndex = guesses.size() - holder.getAdapterPosition() - 1;
                lastDeletedGuessIndex = reverseIndex;

                if (reverseIndex < guesses.size()) {
                    lastDeletedGuess = guesses.get(reverseIndex);
                    guesses.remove(reverseIndex);
                    undoSnackbar.show();
                    notifyItemRemoved(holder.getAdapterPosition());
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return guesses.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView guessTextView;
        ImageButton deleteButton;

        ViewHolder(View view) {
            super(view);
            guessTextView = view.findViewById(R.id.participantTextView);
            deleteButton = view.findViewById(R.id.deleteButton);
        }
    }
}
