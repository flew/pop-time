package grebic.christopher.android.poptime.game.managers;

import java.util.List;

import grebic.christopher.android.poptime.game.logic.Result;

/**
 * Implementation of the {@link ScoreManager}
 * for the {@link grebic.christopher.android.poptime.game.logic.GameMode#TEAM} game mode.
 *
 * @author Christopher Grébic
 */
public class TeamScoreManager extends AbstractScoreManager {
    /**
     * Instantiates a new team score manager.
     *
     * @param teams the number of teams
     */
    public TeamScoreManager(int teams) {
        super(teams);
    }

    /**
     * Score the given results for a specific set and team.
     *
     * @param set     the set
     * @param team    the participant
     * @param results the results
     */
    @Override
    public void score(int set, int team, List<Result> results) {
        int score = countPositiveResults(results);
        List<Integer> setScores = getScores().get(set - 1);
        setScores.set(team, setScores.get(team) + score);
    }
}
