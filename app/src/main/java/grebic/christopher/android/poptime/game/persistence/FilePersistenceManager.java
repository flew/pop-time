package grebic.christopher.android.poptime.game.persistence;

import android.content.Context;
import android.util.Log;

import java.io.File;
import java.io.IOException;
import java.util.List;

import grebic.christopher.android.poptime.game.logic.Guess;
import grebic.christopher.android.poptime.global.StaticConfiguration;

/**
 * The file persistence manager provides application state persistence by using
 * the internal storage of the android device.
 * <p>
 * The data are stored in a file named by the {@link FilePersistenceManager#PERSISTENCE_FILE_NAME} value.
 * <p>
 * The persistence manager maintains a cache of the loaded guesses.
 * This cache is invalidated at each successful {@link FilePersistenceManager#store(List, Context)} calls.
 *
 * @author Christopher Grébic
 */
public class FilePersistenceManager implements PersistenceManager {

    private static final String PERSISTENCE_FILE_NAME = "guesses.dat";

    private List<Guess> cachedList;

    /**
     * Initializes the persistence manager.
     * <p>
     * The {@link FilePersistenceManager#PERSISTENCE_FILE_NAME} is created if it does not exist.
     *
     * @param context the context of the application
     * @throws PersistenceException if the creation of the file failed
     */
    @Override
    public void initialize(Context context) {
        File file = new File(context.getFilesDir(), PERSISTENCE_FILE_NAME);
        if (!file.exists()) {
            try {
                file.createNewFile();
                store(StaticConfiguration.defaultGuesses, context);
            } catch (IOException exception) {
                Log.e(StaticConfiguration.APP, exception.getMessage(), exception);
                throw new PersistenceException("Error creating the persistence file on the internal storage.", exception, context);
            }
        }
    }

    /**
     * Stores a list of guesses into the persistence file overwriting any previously store value.
     *
     * @param guesses the list of guess to be stored
     * @param context the context of the application
     * @throws PersistenceException if an error occurs during the storage
     */
    @Override
    public void store(List<Guess> guesses, Context context) {
        GuessOutputStream out = null;
        try {
            out = new GuessOutputStream(context.openFileOutput(PERSISTENCE_FILE_NAME, Context.MODE_PRIVATE));
            out.writeGuesses(guesses);
            cachedList = null;
        } catch (IOException exception) {
            Log.e(StaticConfiguration.APP, exception.getMessage(), exception);
            throw new PersistenceException(exception, context);
        } finally {
            if (out != null) {
                try {
                    out.close();
                } catch (IOException exception) {
                    Log.e(StaticConfiguration.APP, exception.getMessage(), exception);
                }
            }
        }
    }

    /**
     * Loads a list of guesses from the persistence file.
     * <p>
     * The list of guesses will be returned from the cache after the first call.
     * The cache is invalidated by a {@link FilePersistenceManager#store(List, Context)} call.
     *
     * @param context the context of the application
     * @return the list of guesses
     * @throws PersistenceException if an error occurs during the loading
     */
    @Override
    public List<Guess> load(Context context) {
        if (cachedList != null) {
            return cachedList;
        }

        GuessInputStream in = null;
        try {
            in = new GuessInputStream(context.openFileInput(PERSISTENCE_FILE_NAME));
            cachedList = in.readGuesses();
            return cachedList;
        } catch (IOException exception) {
            Log.e(StaticConfiguration.APP, exception.getMessage(), exception);
            throw new PersistenceException(exception, context);
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException exception) {
                    Log.e(StaticConfiguration.APP, exception.getMessage(), exception);
                }
            }
        }
    }
}
