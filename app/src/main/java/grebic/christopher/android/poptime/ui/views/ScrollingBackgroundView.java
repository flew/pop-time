package grebic.christopher.android.poptime.ui.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;

import grebic.christopher.android.poptime.R;

/**
 * A custom view that is used to have an infinitely scrolling background image.
 * <p>
 * This is view has a purely cosmetic purpose.
 * <p>
 * It has 2 attributes from the <tt>poptime</tt> namespace :
 * <ul>
 * <li>image: a reference to a drawable that will be used as the repeating background.</li>
 * <li>speed: the speed of the scrolling animation. Default is 1.</li>
 * </ul>
 *
 * @author Christopher Grébic
 */
public class ScrollingBackgroundView extends View {

    private Bitmap image;
    private Rect bounds = new Rect();

    private int speed = 0;

    private int offsetX = 0;
    private int offsetY = 0;

    /**
     * Instantiates a new scrolling background view.
     *
     * @param context the context of the application
     * @param attrs   the attributes passed to view
     */
    public ScrollingBackgroundView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);

        TypedArray attributes = context.getTheme().obtainStyledAttributes(attrs, R.styleable.poptime, 0, 0);
        image = BitmapFactory.decodeResource(getResources(), attributes.getResourceId(R.styleable.poptime_image, 0));
        speed = attributes.getInteger(R.styleable.poptime_speed, 1);
    }

    /**
     * Draws the view.
     * <p>
     * The background image is repeated on the X and Y axes to fill the whole canvas.
     *
     * @param canvas the canvas
     */
    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        if (canvas == null || image == null) return;

        canvas.getClipBounds(bounds);

        for (int x = offsetX; x < bounds.width(); x += image.getWidth()) {
            for (int y = offsetY; y < bounds.height(); y += image.getHeight()) {
                canvas.drawBitmap(image, x, y, null);
            }
        }

        if (speed != 0) {
            offsetX -= speed;
            offsetY -= speed;
            postInvalidateOnAnimation();
        }
    }
}
