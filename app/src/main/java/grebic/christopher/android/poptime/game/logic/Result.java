package grebic.christopher.android.poptime.game.logic;

/**
 * Binds a {@link Guess} to a result (positive/negative).
 *
 * @author Christopher Grébic
 */
public class Result {

    private Guess guess;
    private boolean positive;

    /**
     * Creates a negative result for the given guess.
     *
     * @param guess the guess
     */
    public Result(Guess guess) {
        this(guess, false);
    }

    /**
     * Creates a result for the given guess.
     *
     * @param guess    the guess
     * @param positive the result
     */
    public Result(Guess guess, boolean positive) {
        this.guess = guess;
        this.positive = positive;
    }

    /**
     * @return the guess bound to this result
     */
    public Guess getGuess() {
        return guess;
    }

    /**
     * @return true if the result is positive, false otherwise
     */
    public boolean isPositive() {
        return positive;
    }

    /**
     * Sets the result.
     *
     * @param positive the result
     */
    public void setPositive(boolean positive) {
        this.positive = positive;
    }

    /**
     * @return the string value of the bound guess
     */
    @Override
    public String toString() {
        return guess.toString();
    }
}
