package grebic.christopher.android.poptime.ui.activities;

import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import java.util.List;

import grebic.christopher.android.poptime.R;
import grebic.christopher.android.poptime.game.logic.Game;
import grebic.christopher.android.poptime.game.logic.GameMode;
import grebic.christopher.android.poptime.global.GameApplication;
import grebic.christopher.android.poptime.ui.adapters.ScoreListAdapter;

/**
 * Activity displaying the final scores of the game.
 *
 * @author Christopher Grébic
 */
public class FinalActivity extends AbstractAppActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_final);

        Game game = ((GameApplication) getApplication()).getGameManager().getCurrentGame();
        GameMode gameMode = game.getGameMode();
        List<Integer> scores = game.getScoreManager().getTotalScores();
        int winner = game.getScoreManager().getOverallWinner();

        ListView scoreListView = findViewById(R.id.scoreListView);
        scoreListView.setAdapter(new ScoreListAdapter(this, gameMode, scores, winner));

        TextView winnerTextView = findViewById(R.id.winnerTextView);
        if (game.getGameMode() == GameMode.FREE_FOR_FALL) {
            winnerTextView.setText(getResources().getString(R.string.player, winner + 1));
        } else {
            winnerTextView.setText(getResources().getString(R.string.team, winner + 1));
        }
    }

    public void finish(View view) {
        finish();
    }
}
