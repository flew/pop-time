package grebic.christopher.android.poptime.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import grebic.christopher.android.poptime.R;

/**
 * Landing activity. Main menu.
 *
 * @author Christopher Grébic
 */
public class MainActivity extends AbstractAppActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void play(View view) {
        Intent intent = new Intent(this, RulesActivity.class);
        startActivity(intent);
    }

    public void configure(View view) {
        Intent intent = new Intent(this, SettingsActivity.class);
        startActivity(intent);
    }
}
