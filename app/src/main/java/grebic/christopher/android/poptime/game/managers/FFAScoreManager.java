package grebic.christopher.android.poptime.game.managers;

import java.util.List;

import grebic.christopher.android.poptime.game.logic.Result;

/**
 * Implementation of the {@link ScoreManager}
 * for the {@link grebic.christopher.android.poptime.game.logic.GameMode#FREE_FOR_FALL} game mode.
 *
 * @author Christopher Grébic
 */
public class FFAScoreManager extends AbstractScoreManager {
    /**
     * Instantiates a new FFA score manager.
     *
     * @param players the number of teams
     */
    public FFAScoreManager(int players) {
        super(players);
    }

    /**
     * Score the given results for a specific set and player.
     *
     * @param set     the set
     * @param player  the player
     * @param results the results
     */
    @Override
    public void score(int set, int player, List<Result> results) {
        int score = countPositiveResults(results);
        int pair = getPeer(set, player);
        List<Integer> setScores = getScores().get(set - 1);
        setScores.set(player, setScores.get(player) + score);
        setScores.set(pair, setScores.get(pair) + score);
    }

    private int getPeer(int set, int player) {
        int participants = getParticipants();
        if ((set - 1) % 2 == 0) {
            return (player + 1) % participants;
        } else {
            return ((player - 1) % participants + participants) % participants;
        }
    }
}
