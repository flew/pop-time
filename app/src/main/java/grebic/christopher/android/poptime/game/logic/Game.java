package grebic.christopher.android.poptime.game.logic;

import java.util.List;

import grebic.christopher.android.poptime.game.managers.GuessManager;
import grebic.christopher.android.poptime.game.managers.ScoreManager;
import grebic.christopher.android.poptime.global.StaticConfiguration;

/**
 * Represents the actual game being played.
 * <p>
 * Use the {@link GameBuilder} to instantiate a new game.
 *
 * @author Christopher Grébic
 */
public abstract class Game {

    private int participants = 0;
    private int playedSets = 0;
    private int currentParticipant = 0;

    private Turn currentTurn;

    private ScoreManager scoreManager;
    private GuessManager guessManager;

    /**
     * Instantiates a new game.
     *
     * @param participants the number of participants for this game
     * @param scoreManager the score manager used to compute scores for this game
     * @param guessManager the guess manager used to provided guesses for this game
     */
    public Game(int participants, ScoreManager scoreManager, GuessManager guessManager) {
        this.participants = participants;
        this.scoreManager = scoreManager;
        this.guessManager = guessManager;
    }

    /**
     * @return the next participant that should play his turn
     */
    abstract public int getNextParticipant();

    /**
     * @return the game mode
     */
    abstract public GameMode getGameMode();

    /**
     * @return the number of participants
     */
    public int getParticipants() {
        return participants;
    }

    /**
     * @return the currently playing participant
     */
    public int getCurrentParticipant() {
        return currentParticipant;
    }

    /**
     * @return the score manager used to compute scores for this game
     */
    public ScoreManager getScoreManager() {
        return scoreManager;
    }

    /**
     * @return the guess manager used to provided guesses for this game
     */
    public GuessManager getGuessManager() {
        return guessManager;
    }

    /**
     * @return the current turn of the game
     */
    public Turn getCurrentTurn() {
        return currentTurn;
    }

    /**
     * @return the number of sets that have been played
     */
    public int getPlayedSets() {
        return playedSets;
    }

    /**
     * Checks whether the game is over.
     * <p>
     * A game is considered over once {@link StaticConfiguration#SET_COUNT} sets have been played.
     *
     * @return true if the game is over, false otherwise
     */
    public boolean isOver() {
        return playedSets >= StaticConfiguration.SET_COUNT;
    }

    /**
     * Checks whether the current set is over.
     * <p>
     * A set is considered over when all the guesses have been found.
     *
     * @return true if set is over, false otherwise
     */
    public boolean isSetOver() {
        return guessManager.getAvailableGuesses().isEmpty();
    }

    /**
     * Starts a new set.
     * <p>
     * The guesses are shuffled.
     * The participant with the lowest overall score starts. It is the first one for the first set.
     */
    public void set() {
        currentParticipant = scoreManager.getOverallLoser();
        playedSets++;
        guessManager.shuffle();
        currentTurn = new Turn(guessManager);
    }

    /**
     * Starts a new turn for the current set.
     */
    public void turn() {
        currentParticipant = getNextParticipant();
        currentTurn = new Turn(guessManager);
    }

    /**
     * Scores the given results for the current set and participant.
     *
     * @param results the results of the current participant
     */
    public void score(List<Result> results) {
        scoreManager.score(playedSets, currentParticipant, results);
    }
}
