package grebic.christopher.android.poptime.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import grebic.christopher.android.poptime.R;

/**
 * Activity dislaying the quick rules of the game.
 *
 * @author Christopher Grébic
 */
public class RulesActivity extends AbstractAppActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rules);
    }

    public void next(View view) {
        Intent intent = new Intent(this, WizardActivity.class);
        startActivity(intent);
    }

    public void close(View view) {
        finish();
    }
}
