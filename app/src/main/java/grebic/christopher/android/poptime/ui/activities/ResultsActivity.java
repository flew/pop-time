package grebic.christopher.android.poptime.ui.activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.List;

import grebic.christopher.android.poptime.R;
import grebic.christopher.android.poptime.game.logic.Game;
import grebic.christopher.android.poptime.game.logic.Result;
import grebic.christopher.android.poptime.game.managers.GameManager;
import grebic.christopher.android.poptime.global.GameApplication;
import grebic.christopher.android.poptime.ui.adapters.ResultListAdapter;

/**
 * Activity displaying the results of the turn.
 *
 * @author Christopher Grébic
 */
public class ResultsActivity extends AbstractAppActivity {

    private GameManager manager;

    private ListView resultListView;

    private List<Result> results;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_results);

        manager = ((GameApplication) getApplication()).getGameManager();
        results = manager.getCurrentGame().getCurrentTurn().getResults();

        resultListView = findViewById(R.id.resultListView);
        final ResultListAdapter adapter = new ResultListAdapter(this, results);
        resultListView.setAdapter(adapter);

        resultListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int index, long id) {
                Result result = results.get(index);
                result.setPositive(!result.isPositive());
                adapter.notifyDataSetChanged();
            }
        });
    }

    public void next(View view) {
        final Game currentGame = manager.getCurrentGame();
        currentGame.getGuessManager().mergeResults(results);
        currentGame.score(results);
        currentGame.turn();
        Intent intent = new Intent(this, currentGame.isSetOver() ? ScoreActivity.class : SetActivity.class);
        startActivity(intent);
    }

    public void close(View view) {
        new AlertDialog.Builder(this)
                .setTitle(R.string.quit_title)
                .setMessage(R.string.quit_message)
                .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }

                })
                .setNegativeButton(R.string.no, null)
                .show();
    }
}
