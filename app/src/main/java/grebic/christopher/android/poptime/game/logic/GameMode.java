package grebic.christopher.android.poptime.game.logic;

/**
 * Enumeration of the game modes.
 *
 * @author Christopher Grébic
 */
public enum GameMode {
    /**
     * In free for all, each participant takes turn with the one next to them according
     * to the current set order (clockwise-counterclockwise-clockwise).
     * <p>
     * The results are scored for both participant and are added to their overall score
     * at the end of each turn.
     * <p>
     * In this game mode turns are played every two participants.
     */
    FREE_FOR_FALL,

    /**
     * In team, each participant takes turn with their team.
     * <p>
     * The results are scored for the whole team.
     */
    TEAM
}
