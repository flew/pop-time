package grebic.christopher.android.poptime.game.managers;

import grebic.christopher.android.poptime.game.logic.Game;

/**
 * The game manager holds and manage the current game being played.
 *
 * @author Christopher Grébic
 */
public class GameManager {

    private Game currentGame;

    /**
     * @return the current game
     */
    public Game getCurrentGame() {
        return currentGame;
    }

    /**
     * Sets the current game.
     *
     * @param currentGame the current game
     */
    public void setCurrentGame(Game currentGame) {
        this.currentGame = currentGame;
    }
}
