package grebic.christopher.android.poptime.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import grebic.christopher.android.poptime.R;
import grebic.christopher.android.poptime.game.logic.GameMode;

/**
 * A {@link android.widget.ListView} adapter for displaying participants' scores.
 *
 * @author Christopher Grébic
 */
public class ScoreListAdapter extends BaseAdapter {

    private Context context;
    private GameMode gameMode;
    private List<Integer> scores;
    private int winner;

    public ScoreListAdapter(Context context, GameMode gameMode, List<Integer> scores, int winner) {
        this.context = context;
        this.gameMode = gameMode;
        this.scores = scores;
        this.winner = winner;
    }

    @Override
    public int getCount() {
        return scores.size();
    }

    @Override
    public Object getItem(int index) {
        return scores.get(index);
    }

    @Override
    public long getItemId(int index) {
        return index;
    }

    @Override
    public View getView(int index, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.list_item_score, parent, false);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        int score = scores.get(index);

        if (gameMode == GameMode.FREE_FOR_FALL) {
            viewHolder.participantTextView.setText(context.getString(R.string.player, index + 1));
        } else {
            viewHolder.participantTextView.setText(context.getString(R.string.team, index + 1));
        }

        viewHolder.scoreTextView.setText(String.format(context.getResources().getConfiguration().locale, "%d", score));

        if (winner == index) {
            viewHolder.participantTextView.setTextColor(context.getResources().getColor(R.color.colorAccentSecondary));
            viewHolder.scoreTextView.setTextColor(context.getResources().getColor(R.color.colorAccentSecondary));
        }

        return convertView;
    }

    private class ViewHolder {
        TextView participantTextView;
        TextView scoreTextView;

        ViewHolder(View view) {
            participantTextView = view.findViewById(R.id.participantTextView);
            scoreTextView = view.findViewById(R.id.scoreTextView);
        }
    }
}
