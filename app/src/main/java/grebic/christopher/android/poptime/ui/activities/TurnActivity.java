package grebic.christopher.android.poptime.ui.activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.TextView;

import grebic.christopher.android.poptime.R;
import grebic.christopher.android.poptime.game.logic.Game;
import grebic.christopher.android.poptime.game.logic.Turn;
import grebic.christopher.android.poptime.game.managers.GameManager;
import grebic.christopher.android.poptime.game.managers.SoundManager;
import grebic.christopher.android.poptime.global.GameApplication;
import grebic.christopher.android.poptime.global.StaticConfiguration;

/**
 * Activity of a game turn.
 *
 * @author Christopher Grébic
 */
public class TurnActivity extends AbstractAppActivity {

    private TextView timerTextView;
    private TextView guessTextView;
    private Button validButton;
    private Button rejectButton;

    private SoundManager soundManager;
    private Game game;
    private Turn turn;
    private boolean isOver = false;

    private int timer = StaticConfiguration.TURN_TIME;
    private Handler timerHandler = new Handler();
    private Runnable timerRunnable = new Runnable() {
        @Override
        public void run() {
            timer--;
            timerTextView.setText(getResources().getString(R.string.timer, timer));
            if (timer <= 0) {
                soundManager.play(SoundManager.Sound.TIMESUP);
                finishTurn();
            } else {
                if (timer == 5) {
                    soundManager.play(SoundManager.Sound.SEC);
                }
                timerHandler.postDelayed(this, 1000);
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_turn);

        GameManager gameManager = ((GameApplication) getApplication()).getGameManager();
        soundManager = ((GameApplication) getApplication()).getSoundManager();
        game = gameManager.getCurrentGame();
        turn = game.getCurrentTurn();
        isOver = false;

        if (turn.isOver()) {
            finish();
            return;
        }

        timerTextView = findViewById(R.id.timerTextView);
        guessTextView = findViewById(R.id.participantTextView);
        validButton = findViewById(R.id.validButton);
        rejectButton = findViewById(R.id.rejectButton);

        timerTextView.setText(getResources().getString(R.string.timer, StaticConfiguration.TURN_TIME));
        guessTextView.setText(turn.nextGuess().toString());
    }

    @Override
    protected void onStop() {
        super.onStop();

        timerHandler.removeCallbacks(timerRunnable);
    }

    @Override
    protected void onResume() {
        super.onResume();
        soundManager.initialize(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        soundManager.release();
    }

    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        timerHandler.postDelayed(timerRunnable, 1000);
    }

    public void validate(View view) {
        if (isOver) return;

        soundManager.play(SoundManager.Sound.VALID);
        Animation bounceAnimation = AnimationUtils.loadAnimation(this, R.anim.bounce);
        validButton.startAnimation(bounceAnimation);

        turn.validate();
        next();
    }

    public void reject(View view) {
        if (isOver) return;

        soundManager.play(SoundManager.Sound.REJECT);
        Animation bounceAnimation = AnimationUtils.loadAnimation(this, R.anim.bounce);
        rejectButton.startAnimation(bounceAnimation);

        turn.reject();
        next();
    }

    public void finishTurn() {
        isOver = true;
        timerHandler.removeCallbacks(timerRunnable);
        final Intent intent = new Intent(this, ResultsActivity.class);
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                startActivity(intent);
            }
        }, 1000);
    }

    public void close(View view) {
        new AlertDialog.Builder(this)
                .setTitle(R.string.quit_title)
                .setMessage(R.string.quit_message)
                .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }

                })
                .setNegativeButton(R.string.no, null)
                .show();
    }

    private void next() {
        if (turn.isOver()) {
            finishTurn();
        } else {
            guessTextView.setText(turn.nextGuess().toString());
        }
    }
}
