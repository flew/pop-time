package grebic.christopher.android.poptime.ui.fragments;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import grebic.christopher.android.poptime.R;
import grebic.christopher.android.poptime.game.logic.GameBuilder;
import grebic.christopher.android.poptime.global.StaticConfiguration;

/**
 * This fragment displays a number field enabling the user to select how
 * many participants will play.
 *
 * @author Christopher Grébic
 * @see grebic.christopher.android.poptime.game.managers.WizardManager
 */
public class ParticipantFragment extends Fragment {

    private GameBuilder builder;

    private TextView titleTextView;
    private TextView numberTextView;
    private Button plusButton;
    private Button minusButton;

    /**
     * Factory method for creating a new participant fragment.
     *
     * @param builder the builder used for game set up
     * @return a new participant fragment
     */
    public static ParticipantFragment make(GameBuilder builder) {
        ParticipantFragment fragment = new ParticipantFragment();
        fragment.builder = builder;
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_participant, container, false);

        titleTextView = view.findViewById(R.id.titleTextView);

        numberTextView = view.findViewById(R.id.numberTextView);
        numberTextView.setText(String.format(getResources().getConfiguration().locale, "%d", StaticConfiguration.MIN_PARTICIPANTS));

        plusButton = view.findViewById(R.id.plusButton);
        plusButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int count = builder.getParticipants();
                if (count < StaticConfiguration.MAX_PARTICIPANTS) {
                    count++;
                    builder.setParticipants(count);
                    numberTextView.setText(String.format(getResources().getConfiguration().locale, "%d", count));

                    minusButton.setBackground(getResources().getDrawable(R.drawable.white_ripple));
                    if (count == StaticConfiguration.MAX_PARTICIPANTS) {
                        plusButton.setBackgroundColor(getResources().getColor(R.color.colorDisabled));
                    }
                }
            }
        });

        minusButton = view.findViewById(R.id.minusButton);
        minusButton.setBackgroundColor(getResources().getColor(R.color.colorDisabled));
        minusButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int count = builder.getParticipants();
                if (count > StaticConfiguration.MIN_PARTICIPANTS) {
                    count--;
                    builder.setParticipants(count);
                    numberTextView.setText(String.format(getResources().getConfiguration().locale, "%d", count));

                    plusButton.setBackground(getResources().getDrawable(R.drawable.white_ripple));
                    if (count == StaticConfiguration.MIN_PARTICIPANTS) {
                        minusButton.setBackgroundColor(getResources().getColor(R.color.colorDisabled));
                    }
                }
            }
        });

        return view;
    }

    /**
     * Displays the title text corresponding to the selected {@link grebic.christopher.android.poptime.game.logic.GameMode}.
     *
     * @param isVisibleToUser true if this fragment's UI is currently visible to the user (default), false if it is not.
     */
    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);

        if (isVisibleToUser) {
            switch (builder.getGameMode()) {
                case FREE_FOR_FALL:
                    titleTextView.setText(R.string.ffa_participant);
                    break;
                default:
                case TEAM:
                    titleTextView.setText(R.string.team_participant);
                    break;
            }
        }
    }
}
