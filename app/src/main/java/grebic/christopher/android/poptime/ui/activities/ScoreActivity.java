package grebic.christopher.android.poptime.ui.activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import java.util.List;

import grebic.christopher.android.poptime.R;
import grebic.christopher.android.poptime.game.logic.Game;
import grebic.christopher.android.poptime.game.logic.GameMode;
import grebic.christopher.android.poptime.global.GameApplication;
import grebic.christopher.android.poptime.ui.adapters.ScoreListAdapter;

/**
 * Activity displaying the score of the set.
 *
 * @author Christopher Grébic
 */
public class ScoreActivity extends AbstractAppActivity {

    private Game game;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_score);

        game = ((GameApplication) getApplication()).getGameManager().getCurrentGame();
        GameMode gameMode = game.getGameMode();
        List<Integer> scores = game.getScoreManager().getSetScores(game.getPlayedSets());
        int winner = game.getScoreManager().getWinner(game.getPlayedSets());

        ListView scoreListView = findViewById(R.id.scoreListView);
        scoreListView.setAdapter(new ScoreListAdapter(this, gameMode, scores, winner));

        TextView setTextView = findViewById(R.id.setTextView);
        Resources resources = getResources();
        setTextView.setText(resources.getString(resources.getIdentifier("set_" + game.getPlayedSets(), "string", getPackageName())));
    }

    public void next(View view) {
        Intent intent;
        if (game.isOver()) {
            intent = new Intent(this, FinalActivity.class);
        } else {
            intent = new Intent(this, SetActivity.class);
        }
        startActivity(intent);
    }

    public void close(View view) {
        new AlertDialog.Builder(this)
                .setTitle(R.string.quit_title)
                .setMessage(R.string.quit_message)
                .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }

                })
                .setNegativeButton(R.string.no, null)
                .show();
    }
}
