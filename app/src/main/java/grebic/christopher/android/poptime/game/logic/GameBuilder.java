package grebic.christopher.android.poptime.game.logic;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import grebic.christopher.android.poptime.game.managers.FFAScoreManager;
import grebic.christopher.android.poptime.game.managers.GuessManager;
import grebic.christopher.android.poptime.game.managers.TeamScoreManager;
import grebic.christopher.android.poptime.global.GameApplication;
import grebic.christopher.android.poptime.global.StaticConfiguration;

/**
 * A builder class to instantiate {@link Game} objects.
 * <p>
 * The default game is a {@link GameMode#TEAM} game with
 * {@link StaticConfiguration#MAX_PARTICIPANTS} participants.
 *
 * @author Christopher Grébic
 */
public class GameBuilder {

    private List<Guess> possiblesGuesses;
    private GameMode gameMode = GameMode.TEAM;
    private int participants = StaticConfiguration.MIN_PARTICIPANTS;

    /**
     * Instantiates a new game builder with a list of possible guesses.
     * <p>
     * Only {@link StaticConfiguration#GUESS_COUNT} will be taken from the given list.
     *
     * @param possiblesGuesses the possible guesses
     */
    public GameBuilder(List<Guess> possiblesGuesses) {
        this.possiblesGuesses = possiblesGuesses;
    }

    /**
     * @return the game mode
     */
    public GameMode getGameMode() {
        return gameMode;
    }

    /**
     * Sets the game mode.
     *
     * @param gameMode the game mode
     */
    public void setGameMode(GameMode gameMode) {
        this.gameMode = gameMode;
    }

    /**
     * @return the number of participants
     */
    public int getParticipants() {
        return participants;
    }

    /**
     * Sets the number of participants.
     *
     * @param participants the number of participants
     */
    public void setParticipants(int participants) {
        this.participants = participants;
    }

    /**
     * Builds the game.
     *
     * @return the game
     */
    public Game build() {
        List<Guess> guesses;
        if (possiblesGuesses.size() > StaticConfiguration.GUESS_COUNT) {
            guesses = generateRandomSubset(StaticConfiguration.GUESS_COUNT);
        } else {
            guesses = new ArrayList<>(possiblesGuesses);
        }
        GuessManager guessManager = new GuessManager(guesses);

        if (gameMode == GameMode.FREE_FOR_FALL) {
            return new FFAGame(participants, new FFAScoreManager(participants), guessManager);
        } else {
            return new TeamGame(participants, new TeamScoreManager(participants), guessManager);
        }
    }

    /**
     * Generates a subset of the possible guesses;
     *
     * @param size the size of the subset
     * @return the subset
     */
    private List<Guess> generateRandomSubset(int size) {
        HashSet<Guess> set = new HashSet<>(size);
        while (set.size() < size) {
            set.add(possiblesGuesses.get(GameApplication.PRNG.nextInt(possiblesGuesses.size())));
        }
        return new ArrayList<>(set);
    }

}
