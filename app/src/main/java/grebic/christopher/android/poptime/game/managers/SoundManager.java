package grebic.christopher.android.poptime.game.managers;


import android.content.Context;
import android.media.AudioManager;
import android.media.SoundPool;
import android.util.Log;

import java.util.EnumMap;
import java.util.Map;

import grebic.christopher.android.poptime.R;
import grebic.christopher.android.poptime.global.StaticConfiguration;

/**
 * The sound manager enables the loading and playback of stream throughout the game.
 * <p>
 * The sound effects list is represented by the {@link Sound} enumeration.
 * All sounds are loaded at the initialization and are kept in a map for reference.
 * <p>
 * You should call the {@link SoundManager#release()} method once you no longer to play the sounds
 * in order to free the resources.
 *
 * @author Christopher Grébic
 */
public class SoundManager {
    private SoundPool soundPool;
    private Map<Sound, Integer> sfxMap;

    /**
     * Instantiates a new sound manager.
     */
    public SoundManager() {
        sfxMap = new EnumMap<>(Sound.class);
    }

    /**
     * Initializes the sound manager.
     * <p>
     * All sound effects are loaded into memory.
     *
     * @param context the context of the application
     */
    public void initialize(Context context) {
        if (soundPool == null) {
            soundPool = new SoundPool(4, AudioManager.STREAM_MUSIC, 0);
            sfxMap.put(Sound.SEC, soundPool.load(context, R.raw.sec, 1));
            sfxMap.put(Sound.TIMESUP, soundPool.load(context, R.raw.timesup, 1));
            sfxMap.put(Sound.VALID, soundPool.load(context, R.raw.valid, 1));
            sfxMap.put(Sound.REJECT, soundPool.load(context, R.raw.reject, 1));
        }
    }

    /**
     * Releases all sounds managed by the sound manager.
     */
    public void release() {
        soundPool.release();
        soundPool = null;
    }

    /**
     * Play the given sound effect.
     *
     * @param sound the sound effect to be played
     */
    public void play(Sound sound) {
        if (soundPool.play(sfxMap.get(sound), 1, 1, 0, 0, 1) == 0) {
            Log.w(StaticConfiguration.APP, "Playback of the " + sound.name() + " SFX failed.");
        }
    }

    /**
     * The sound effects list
     */
    public enum Sound {
        SEC,
        TIMESUP,
        VALID,
        REJECT
    }

}
