package grebic.christopher.android.poptime.game.managers;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import grebic.christopher.android.poptime.game.logic.Result;
import grebic.christopher.android.poptime.global.StaticConfiguration;

/**
 * Partial implementation of the {@link ScoreManager}.
 *
 * @author Christopher Grébic
 */
abstract class AbstractScoreManager implements ScoreManager {

    private int participants;
    private List<List<Integer>> scores;

    /**
     * Constructs the AbstractScoreManager.
     * <p>
     * The scores are set to 0 for each participant and set.
     *
     * @param participants the number of participants
     */
    AbstractScoreManager(int participants) {
        this.participants = participants;
        scores = new ArrayList<>(StaticConfiguration.SET_COUNT);
        for (int i = 0; i < StaticConfiguration.SET_COUNT; i++) {
            scores.add(createEmptyScoreList());
        }
    }

    /**
     * @return the number of participants
     */
    protected int getParticipants() {
        return participants;
    }

    /**
     * @return the scores by participant by set
     */
    protected List<List<Integer>> getScores() {
        return scores;
    }

    /**
     * Returns the participants' scores for a given set.
     *
     * @param set the set
     * @return the scores of the set
     */
    @Override
    public List<Integer> getSetScores(int set) {
        return scores.get(set - 1);
    }

    /**
     * @return the participants' total scores.
     */
    @Override
    public List<Integer> getTotalScores() {
        List<Integer> totalScores = createEmptyScoreList();
        for (int set = 0; set < scores.size(); set++) {
            for (int partticipant = 0; partticipant < participants; partticipant++) {
                totalScores.set(partticipant, totalScores.get(partticipant) + scores.get(set).get(partticipant));
            }
        }
        return totalScores;
    }

    /**
     * Returns the winner of the given set.
     *
     * @param set the set
     * @return the winner of the set
     */
    @Override
    public int getWinner(int set) {
        if (set == 0) return 0;

        List<Integer> setScores = scores.get(set - 1);
        int winner = 0;
        for (int participant = 0; participant < setScores.size(); participant++) {
            if (setScores.get(participant) > setScores.get(winner)) {
                winner = participant;
            }
        }
        return winner;
    }

    /**
     * Returns the loser of the given set.
     *
     * @param set the set
     * @return the loser of the set
     */
    @Override
    public int getLoser(int set) {
        if (set == 0) return 0;

        List<Integer> setScores = scores.get(set - 1);
        int loser = 0;
        for (int participant = 0; participant < setScores.size(); participant++) {
            if (setScores.get(participant) < setScores.get(loser)) {
                loser = participant;
            }
        }
        return loser;
    }

    /**
     * @return the currently winning participant of the game
     */
    @Override
    public int getOverallWinner() {
        List<Integer> scores = getTotalScores();
        int winner = 0;
        for (int participant = 0; participant < scores.size(); participant++) {
            if (scores.get(participant) > scores.get(winner)) {
                winner = participant;
            }
        }
        return winner;
    }

    /**
     * @return the currently losing participant of the game
     */
    @Override
    public int getOverallLoser() {
        List<Integer> scores = getTotalScores();
        int loser = 0;
        for (int participant = 0; participant < scores.size(); participant++) {
            if (scores.get(participant) < scores.get(loser)) {
                loser = participant;
            }
        }
        return loser;
    }

    /**
     * Counts the positive results from the given list.
     *
     * @param results the list of results
     * @return the number of positive results
     */
    protected int countPositiveResults(List<Result> results) {
        int count = 0;
        for (Result result : results) {
            if (result.isPositive()) {
                count++;
            }
        }
        return count;
    }

    private List<Integer> createEmptyScoreList() {
        return new ArrayList<>(Collections.nCopies(participants, 0));
    }
}
