package grebic.christopher.android.poptime.game.managers;

import java.util.List;

import grebic.christopher.android.poptime.game.logic.Result;

/**
 * The score manager keeps tab of the score of each participant for each set.
 * It also handle the computing of those score from results list.
 *
 * @author Christopher Grébic
 */
public interface ScoreManager {
    /**
     * Score the given results for a specific set and participant.
     *
     * @param set         the set
     * @param participant the participant
     * @param results     the results
     */
    void score(int set, int participant, List<Result> results);

    /**
     * Returns the participants' scores for a given set.
     *
     * @param set the set
     * @return the scores of the set
     */
    List<Integer> getSetScores(int set);

    /**
     * @return the participants' total scores.
     */
    List<Integer> getTotalScores();

    /**
     * Returns the winner of the given set.
     *
     * @param set the set
     * @return the winner of the set
     */
    int getWinner(int set);

    /**
     * Returns the loser of the given set.
     *
     * @param set the set
     * @return the loser of the set
     */
    int getLoser(int set);

    /**
     * @return the currently winning participant of the game
     */
    int getOverallWinner();

    /**
     * @return the currently losing participant of the game
     */
    int getOverallLoser();
}
