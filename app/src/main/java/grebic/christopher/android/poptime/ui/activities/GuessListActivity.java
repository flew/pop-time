package grebic.christopher.android.poptime.ui.activities;

import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;

import java.util.List;

import grebic.christopher.android.poptime.R;
import grebic.christopher.android.poptime.game.logic.Guess;
import grebic.christopher.android.poptime.global.GameApplication;
import grebic.christopher.android.poptime.ui.adapters.GuessListAdapter;

/**
 * Activity for the management of the possible guesses.
 *
 * @author Christopher Grébic
 */
public class GuessListActivity extends AbstractAppActivity {

    private GameApplication application;

    private List<Guess> guesses;

    private Runnable hideRunnable = new Runnable() {
        @Override
        public void run() {
            setUpFullscreen();
        }
    };
    private Handler hideHandler = new Handler();

    private EditText guessNameField;
    private RecyclerView guessRecyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_guess_list);

        application = (GameApplication) getApplication();
        guesses = application.getPersistenceManager().load(this);

        View main = findViewById(R.id.main);
        guessRecyclerView = findViewById(R.id.guessRecyclerView);
        guessRecyclerView.setHasFixedSize(true);
        guessRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        guessRecyclerView.setAdapter(new GuessListAdapter(this, main, guesses));

        guessNameField = findViewById(R.id.guessNameField);
        guessNameField.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (hasFocus) {
                    hideHandler.removeCallbacks(hideRunnable);
                    hideHandler.postDelayed(hideRunnable, 300);
                }
            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();
        application.getPersistenceManager().store(guesses, this);
    }

    public void back(View view) {
        finish();
    }

    public void add(View view) {
        String name = guessNameField.getText().toString();
        if (!name.isEmpty()) {
            guesses.add(new Guess(name));
            guessRecyclerView.getAdapter().notifyItemInserted(0);
            guessRecyclerView.scrollToPosition(0);
            guessNameField.setText("");
        }
    }
}
