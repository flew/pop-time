package grebic.christopher.android.poptime.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import grebic.christopher.android.poptime.R;
import grebic.christopher.android.poptime.game.logic.Result;

/**
 * A {@link android.widget.ListView} adapter for displaying {@link Result}.
 *
 * @author Christopher Grébic
 */
public class ResultListAdapter extends BaseAdapter {

    private Context context;
    private List<Result> results;

    public ResultListAdapter(Context context, List<Result> results) {
        this.context = context;
        this.results = results;
    }

    @Override
    public int getCount() {
        return results.size();
    }

    @Override
    public Object getItem(int index) {
        return results.get(index);
    }

    @Override
    public long getItemId(int index) {
        return index;
    }

    @Override
    public View getView(int index, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.list_item_result, parent, false);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        Result result = results.get(index);
        int color = result.isPositive() ? R.color.colorValid : R.color.colorReject;

        viewHolder.resultTextView.setText(result.getGuess().toString());
        viewHolder.resultTextView.setTextColor(context.getResources().getColor(color));

        viewHolder.statusTextView.setText(result.isPositive() ? "✓" : "✘");
        viewHolder.statusTextView.setTextColor(context.getResources().getColor(color));

        return convertView;
    }

    private class ViewHolder {
        TextView resultTextView;
        TextView statusTextView;

        ViewHolder(View view) {
            resultTextView = view.findViewById(R.id.participantTextView);
            statusTextView = view.findViewById(R.id.scoreTextView);
        }
    }
}
