package grebic.christopher.android.poptime.game.logic;

import grebic.christopher.android.poptime.game.managers.FFAScoreManager;
import grebic.christopher.android.poptime.game.managers.GuessManager;

/**
 * Implementation of {@link Game} for the {@link GameMode#FREE_FOR_FALL} game mode.
 * <p>
 * Participants take turns with the one next to them according
 * to the current set order (clockwise-counterclockwise-clockwise).
 *
 * @author Christopher Grébic
 * @see TeamGame
 * @see GameBuilder
 */
class FFAGame extends Game {
    /**
     * Instantiates a new team game.
     *
     * @param participants the number of participants for this game
     * @param scoreManager the score manager used to compute scores for this game
     * @param guessManager the guess manager used to provided guesses for this game
     */
    FFAGame(int participants, FFAScoreManager scoreManager, GuessManager guessManager) {
        super(participants, scoreManager, guessManager);
    }

    /**
     * @return the next participant that should play his turn
     */
    @Override
    public int getNextParticipant() {
        int participant = getCurrentParticipant();
        int participants = getParticipants();
        return (getPlayedSets() - 1) % 2 == 0 ? (participant + 2) % participants : ((participant - 2) % participants + participants) % participants;
    }

    /**
     * @return {@link GameMode#FREE_FOR_FALL}
     */
    @Override
    public GameMode getGameMode() {
        return GameMode.FREE_FOR_FALL;
    }

}
