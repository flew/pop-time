package grebic.christopher.android.poptime.game.persistence;


import android.content.Context;

import java.util.List;

import grebic.christopher.android.poptime.game.logic.Guess;

/**
 * The persistence manager is responsible for the storage and loading of the
 * application state. For the moment the application only need to persist
 * a list of {@link Guess}.
 * <p>
 * A persistence manager should throw a {@link PersistenceException} when an error occurs.
 *
 * @author Christopher Grébic
 */
public interface PersistenceManager {
    /**
     * Initializes the persistence manager.
     * <p>
     * This method should set up the persistence before any storage or loading, e.g. connecting
     * to a database. It can be called more than once during the application life time.
     *
     * @param context the context of the application
     * @throws PersistenceException if an error occurs during the initialization
     */
    void initialize(Context context);

    /**
     * Stores a list of guesses.
     * <p>
     * It should overwrite any previously stored list for the same context.
     *
     * @param guesses the list of guess to be stored
     * @param context the context of the application
     * @throws PersistenceException if an error occurs during the storage
     */
    void store(List<Guess> guesses, Context context);

    /**
     * Loads a list of guesses for a given context
     *
     * @param context the context of the application
     * @return the list of guesses
     * @throws PersistenceException if an error occurs during the loading
     */
    List<Guess> load(Context context);
}
