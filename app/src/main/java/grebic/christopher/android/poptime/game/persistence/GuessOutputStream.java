package grebic.christopher.android.poptime.game.persistence;

import android.support.annotation.NonNull;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;

import grebic.christopher.android.poptime.game.logic.Guess;

/**
 * An {@link OutputStream} implementation to writes single or multiple {@link Guess} to a stream.
 * <p>
 * The stream use an underlying {@link DataOutputStream} for primitive and string writing.
 *
 * @author Christopher Grébic
 */
class GuessOutputStream extends OutputStream {

    private DataOutputStream dataOutputStream;

    /**
     * Creates a new GuessOutputStream that will write to the given output stream.
     *
     * @param out the output stream
     */
    public GuessOutputStream(OutputStream out) {
        dataOutputStream = new DataOutputStream(out);
    }

    /**
     * Writes a guess to the output stream.
     *
     * @param guess the guess
     * @throws IOException if an I/O error occurs
     */
    public void writeGuess(@NonNull Guess guess) throws IOException {
        dataOutputStream.writeUTF(guess.toString());
    }

    /**
     * Writes a list of guess to the output stream.
     *
     * @param guesses the guesses
     * @throws IOException if an I/O error occurs
     */
    public void writeGuesses(@NonNull List<Guess> guesses) throws IOException {
        dataOutputStream.writeInt(guesses.size());
        for (Guess guess : guesses) {
            writeGuess(guess);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void write(@NonNull byte[] b) throws IOException {
        dataOutputStream.write(b);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void write(@NonNull byte[] b, int off, int len) throws IOException {
        dataOutputStream.write(b, off, len);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void flush() throws IOException {
        dataOutputStream.flush();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void close() throws IOException {
        dataOutputStream.close();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void write(int b) throws IOException {
        dataOutputStream.write(b);
    }
}
