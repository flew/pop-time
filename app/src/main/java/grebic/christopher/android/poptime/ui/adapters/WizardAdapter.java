package grebic.christopher.android.poptime.ui.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import grebic.christopher.android.poptime.game.managers.WizardManager;


/**
 * A {@link android.support.v4.view.ViewPager} adapter for displaying steps of the game wizard.
 *
 * @author Christopher Grébic
 * @see WizardManager
 * @see grebic.christopher.android.poptime.ui.fragments.GameModeFragment
 * @see grebic.christopher.android.poptime.ui.fragments.ParticipantFragment
 */
public class WizardAdapter extends FragmentStatePagerAdapter {

    private WizardManager manager;

    public WizardAdapter(FragmentManager fragmentManager, WizardManager manager) {
        super(fragmentManager);
        this.manager = manager;
    }

    @Override
    public Fragment getItem(int position) {
        return manager.getStepFragment(position);
    }

    @Override
    public int getCount() {
        return WizardManager.WIZARD_STEP_COUNT;
    }
}
