package grebic.christopher.android.poptime.ui.activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import grebic.christopher.android.poptime.R;
import grebic.christopher.android.poptime.game.logic.Game;
import grebic.christopher.android.poptime.game.logic.GameMode;
import grebic.christopher.android.poptime.game.managers.GameManager;
import grebic.christopher.android.poptime.global.GameApplication;

/**
 * Activity displaying the details of the current set.
 *
 * @author Christopher Grébic
 */
public class SetActivity extends AbstractAppActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_set);

        GameManager manager = ((GameApplication) getApplication()).getGameManager();
        Game game = manager.getCurrentGame();

        if (game.isSetOver()) {
            game.set();
        }

        Resources resources = getResources();
        TextView setTextView = findViewById(R.id.setTextView);
        setTextView.setText(resources.getString(resources.getIdentifier("set_" + game.getPlayedSets(), "string", getPackageName())));
        TextView ruleTextView = findViewById(R.id.ruleTextView);
        ruleTextView.setText(resources.getString(resources.getIdentifier("set_" + game.getPlayedSets() + "_rule", "string", getPackageName())));

        TextView participantTextView = findViewById(R.id.participantTextView);
        int participant = game.getCurrentParticipant();
        if (game.getGameMode() == GameMode.FREE_FOR_FALL) {
            participantTextView.setText(getString(R.string.player, participant + 1));
        } else {

            participantTextView.setText(getString(R.string.team, participant + 1));
        }
    }

    public void turn(View view) {
        Intent intent = new Intent(this, TurnActivity.class);
        startActivity(intent);
    }

    public void close(View view) {
        new AlertDialog.Builder(this)
                .setTitle(R.string.quit_title)
                .setMessage(R.string.quit_message)
                .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }

                })
                .setNegativeButton(R.string.no, null)
                .show();
    }
}
