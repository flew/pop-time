package grebic.christopher.android.poptime.ui.fragments;

import android.graphics.drawable.TransitionDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import grebic.christopher.android.poptime.R;
import grebic.christopher.android.poptime.game.logic.GameBuilder;
import grebic.christopher.android.poptime.game.logic.GameMode;

/**
 * This fragment enables the user to select the {@link GameMode} of the game.
 *
 * @author Christopher Grébic
 * @see grebic.christopher.android.poptime.game.managers.WizardManager
 */
public class GameModeFragment extends Fragment {

    private static final int TRANSITION_DURATION = 300;

    private GameBuilder builder;

    private Button teamButton;
    private Button ffaButton;

    /**
     * Factory method for creating a new game mode fragment.
     *
     * @param builder the builder used for game set up
     * @return a new participant fragment
     */
    public static GameModeFragment make(GameBuilder builder) {
        GameModeFragment fragment = new GameModeFragment();
        fragment.builder = builder;
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_gamemode, container, false);

        teamButton = view.findViewById(R.id.teamButton);
        teamButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (builder.getGameMode() != GameMode.TEAM) {
                    builder.setGameMode(GameMode.TEAM);

                    TransitionDrawable teamDrawable = (TransitionDrawable) teamButton.getBackground();
                    teamDrawable.startTransition(TRANSITION_DURATION);

                    TransitionDrawable ffaDrawable = (TransitionDrawable) ffaButton.getBackground();
                    ffaDrawable.reverseTransition(TRANSITION_DURATION);
                }
            }
        });


        ffaButton = view.findViewById(R.id.ffaButton);
        ffaButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (builder.getGameMode() != GameMode.FREE_FOR_FALL) {
                    builder.setGameMode(GameMode.FREE_FOR_FALL);

                    TransitionDrawable drawable = (TransitionDrawable) ffaButton.getBackground();
                    drawable.startTransition(TRANSITION_DURATION);

                    TransitionDrawable teamDrawable = (TransitionDrawable) teamButton.getBackground();
                    teamDrawable.reverseTransition(TRANSITION_DURATION);
                }
            }
        });

        if (builder.getGameMode() == GameMode.TEAM) {
            TransitionDrawable teamDrawable = (TransitionDrawable) teamButton.getBackground();
            teamDrawable.startTransition(0);
        } else {
            TransitionDrawable drawable = (TransitionDrawable) ffaButton.getBackground();
            drawable.startTransition(0);
        }

        return view;
    }
}
