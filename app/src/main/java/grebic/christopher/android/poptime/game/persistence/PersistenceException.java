package grebic.christopher.android.poptime.game.persistence;

import android.content.Context;

/**
 * The persistence exception is used to encapsulate exceptions thrown by a persistence manager.
 *
 * @author Christopher Grébic
 */
public class PersistenceException extends RuntimeException {

    private Context context;

    /**
     * Instantiates a new persistence exception.
     */
    public PersistenceException() {
    }

    /**
     * Instantiates a new persistence exception.
     *
     * @param message the message of the exception
     */
    public PersistenceException(String message) {
        super(message);
    }

    /**
     * Instantiates a new persistence exception.
     *
     * @param message the message of the exception
     * @param cause   the cause of the exception
     */
    public PersistenceException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * Instantiates a new persistence exception.
     *
     * @param cause the cause of the exception
     */
    public PersistenceException(Throwable cause) {
        super(cause);
    }

    /**
     * Instantiates a new persistence exception with the current context.
     *
     * @param context the context of the application
     */
    public PersistenceException(Context context) {
        this.context = context;
    }

    /**
     * Instantiates a new persistence exception with the current context.
     *
     * @param message the message of the exception
     * @param context the context of the application
     */
    public PersistenceException(String message, Context context) {
        super(message);
        this.context = context;
    }

    /**
     * Instantiates a new persistence exception with the current context.
     *
     * @param message the message of the exception
     * @param cause   the cause of the exception
     * @param context the context of the application
     */
    public PersistenceException(String message, Throwable cause, Context context) {
        super(message, cause);
        this.context = context;
    }

    /**
     * Instantiates a new persistence exception with the current context.
     *
     * @param cause   the cause of the exception
     * @param context the context of the application
     */
    public PersistenceException(Throwable cause, Context context) {
        super(cause);
        this.context = context;
    }

    /**
     * @return the context of the application
     */
    public Context getContext() {
        return context;
    }
}
