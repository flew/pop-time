package grebic.christopher.android.poptime.global;

import android.app.Application;
import android.util.Log;

import java.util.Random;

import grebic.christopher.android.poptime.game.managers.GameManager;
import grebic.christopher.android.poptime.game.managers.SoundManager;
import grebic.christopher.android.poptime.game.persistence.FilePersistenceManager;
import grebic.christopher.android.poptime.game.persistence.PersistenceManager;

/**
 * The game application class holds the context of the whole application
 * so that it can be shared (e.g. activities).
 *
 * @author Christopher Grébic
 */
public class GameApplication extends Application {
    /**
     * Pseudo-random number generator for the whole application
     */
    public static final Random PRNG = new Random();

    private PersistenceManager persistenceManager;
    private GameManager gameManager;
    private SoundManager soundManager;

    /**
     * Initializes the game application.
     * The game, persistence and sound managers are instantiated and set up.
     */
    @Override
    public void onCreate() {
        super.onCreate();

        gameManager = new GameManager();

        persistenceManager = new FilePersistenceManager();
        persistenceManager.initialize(this);

        soundManager = new SoundManager();

        Log.i(StaticConfiguration.APP, "Game application initialized.");
    }

    /**
     * @return the game manager
     */
    public GameManager getGameManager() {
        return gameManager;
    }

    /**
     * @return the persistence manager
     */
    public PersistenceManager getPersistenceManager() {
        return persistenceManager;
    }

    /**
     * @return the sound manager
     */
    public SoundManager getSoundManager() {
        return soundManager;
    }

    /**
     * Resets the application.
     * <p>
     * This forces the persistence manager to be reinitialized and to overwrite the current persisted entities.
     * The current game is also discarded if there is any.
     */
    public void reset() {
        persistenceManager.initialize(this);
        persistenceManager.store(StaticConfiguration.defaultGuesses, this);
        gameManager.setCurrentGame(null);
        Log.i(StaticConfiguration.APP, "The application has been reset by the user.");
    }
}
