package grebic.christopher.android.poptime.ui.activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import grebic.christopher.android.poptime.R;
import grebic.christopher.android.poptime.global.GameApplication;

/**
 * Activity displaying the application settings.
 *
 * @author Christopher Grébic
 */
public class SettingsActivity extends AbstractAppActivity {

    private GameApplication application;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        application = (GameApplication) getApplication();
    }

    public void manage(View view) {
        Intent intent = new Intent(this, GuessListActivity.class);
        startActivity(intent);
    }

    public void reset(View view) {
        new AlertDialog.Builder(this)
                .setTitle(R.string.reset_title)
                .setMessage(R.string.reset_message)
                .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        application.reset();
                    }

                })
                .setNegativeButton(R.string.no, null)
                .show();
    }

    public void back(View view) {
        finish();
    }
}
