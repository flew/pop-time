package grebic.christopher.android.poptime.game.logic;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;

import grebic.christopher.android.poptime.game.managers.GuessManager;

/**
 * Represents an actual turn of the game.
 * <p>
 * A turn is played by a participant that can validate or reject guesses that are presented to him.
 * The turn iterates over the possible guesses provied by the {@link GuessManager}.
 * <p>
 * Each validation or rejection are stored in a list as a {@link Result}.
 *
 * @author Christopher Grébic
 */
public class Turn {

    private Iterator<Guess> guessIterator;
    private List<Result> results;

    /**
     * Creates a new turn.
     * <p>
     * The turn uses an iterator of the possible guess provided by the guess manager.
     *
     * @param guessManager the guess manager
     */
    public Turn(GuessManager guessManager) {
        results = new ArrayList<>();
        guessIterator = guessManager.getAvailableGuesses().iterator();
    }

    /**
     * Checks whether the turn is over or not.
     * <p>
     * A turn is considered over when no more guess are available.
     *
     * @return true if the turn is over, false otherwise
     */
    public boolean isOver() {
        return !guessIterator.hasNext();
    }

    /**
     * @return the results of the turn
     */
    public List<Result> getResults() {
        return results;
    }

    /**
     * Returns the next available guess.
     * <p>
     * The guess is removed from the available guesses and added as a result.
     *
     * @return the guess
     * @throws NoSuchElementException if there is no more guess available
     */
    public Guess nextGuess() {
        if (!guessIterator.hasNext()) {
            throw new NoSuchElementException();
        }

        Guess guess = guessIterator.next();
        guessIterator.remove();
        results.add(new Result(guess));
        return guess;
    }

    /**
     * Validates the last guess.
     */
    public void validate() {
        results.get(results.size() - 1).setPositive(true);
    }

    /**
     * Rejects the last guess.
     */
    public void reject() {
        results.get(results.size() - 1).setPositive(false);
    }
}
