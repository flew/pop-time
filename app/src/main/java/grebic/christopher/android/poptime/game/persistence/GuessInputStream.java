package grebic.christopher.android.poptime.game.persistence;

import android.support.annotation.NonNull;

import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import grebic.christopher.android.poptime.game.logic.Guess;

/**
 * An {@link InputStream} implementation to read single or multiple {@link Guess} from a stream.
 * <p>
 * The stream use an underlying {@link DataInputStream} for primitive and string reading.
 *
 * @author Christopher Grébic
 */
class GuessInputStream extends InputStream {

    private DataInputStream dataInputStream;

    /**
     * Creates a new GuessInputStream that will read from the given input stream.
     *
     * @param in the input stream
     */
    public GuessInputStream(InputStream in) {
        this.dataInputStream = new DataInputStream(in);
    }

    /**
     * Reads a guess from the input stream.
     *
     * @return the guess
     * @throws IOException if an I/O error occurs
     */
    public Guess readGuess() throws IOException {
        String value = dataInputStream.readUTF();
        return new Guess(value);
    }

    /**
     * Reads a list of guesses from the input stream.
     *
     * @return the list of guesses
     * @throws IOException if an I/O error occurs
     */
    public List<Guess> readGuesses() throws IOException {
        int count = dataInputStream.readInt();
        List<Guess> guesses = new ArrayList<>(count);
        for (int i = 0; i < count; i++) {
            guesses.add(readGuess());
        }
        return guesses;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int read() throws IOException {
        return dataInputStream.read();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int read(@NonNull byte[] b) throws IOException {
        return dataInputStream.read(b);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int read(@NonNull byte[] b, int off, int len) throws IOException {
        return dataInputStream.read(b, off, len);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public long skip(long n) throws IOException {
        return dataInputStream.skip(n);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int available() throws IOException {
        return dataInputStream.available();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void close() throws IOException {
        dataInputStream.close();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public synchronized void mark(int readlimit) {
        dataInputStream.mark(readlimit);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public synchronized void reset() throws IOException {
        dataInputStream.reset();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean markSupported() {
        return dataInputStream.markSupported();
    }
}
