package grebic.christopher.android.poptime.game.logic;


import grebic.christopher.android.poptime.game.managers.GuessManager;
import grebic.christopher.android.poptime.game.managers.TeamScoreManager;

/**
 * Implementation of {@link Game} for the {@link GameMode#TEAM} game mode.
 * <p>
 * Participants take turns sequentially clockwise.
 *
 * @author Christopher Grébic
 * @see FFAGame
 * @see GameBuilder
 */
class TeamGame extends Game {
    /**
     * Instantiates a new team game.
     *
     * @param participants the number of participants for this game
     * @param scoreManager the score manager used to compute scores for this game
     * @param guessManager the guess manager used to provided guesses for this game
     */
    TeamGame(int participants, TeamScoreManager scoreManager, GuessManager guessManager) {
        super(participants, scoreManager, guessManager);
    }

    /**
     * @return the next participant that should play his turn
     */
    @Override
    public int getNextParticipant() {
        return (getCurrentParticipant() + 1) % getParticipants();
    }

    /**
     * @return {@link GameMode#TEAM}
     */
    @Override
    public GameMode getGameMode() {
        return GameMode.TEAM;
    }
}
