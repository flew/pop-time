package grebic.christopher.android.poptime.ui.activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.View;

import java.util.List;

import grebic.christopher.android.poptime.R;
import grebic.christopher.android.poptime.game.logic.GameBuilder;
import grebic.christopher.android.poptime.game.logic.Guess;
import grebic.christopher.android.poptime.game.managers.GameManager;
import grebic.christopher.android.poptime.game.managers.WizardManager;
import grebic.christopher.android.poptime.game.persistence.PersistenceManager;
import grebic.christopher.android.poptime.global.GameApplication;
import grebic.christopher.android.poptime.ui.adapters.WizardAdapter;

/**
 * Activity handling the creation of a new game.
 *
 * @author Christopher Grébic
 */
public class WizardActivity extends AbstractAppActivity {

    private GameManager gameManager;
    private WizardManager wizardManager;

    private boolean available;

    private ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wizard);

        PersistenceManager persistenceManager = ((GameApplication) getApplication()).getPersistenceManager();
        List<Guess> guesses = persistenceManager.load(this);
        available = guesses.size() > 0;

        gameManager = ((GameApplication) getApplication()).getGameManager();
        wizardManager = new WizardManager(new GameBuilder(guesses));

        viewPager = findViewById(R.id.viewPager);
        viewPager.setAdapter(new WizardAdapter(getSupportFragmentManager(), wizardManager));
    }

    public void next(View view) {
        if (!available) {
            new AlertDialog.Builder(this)
                    .setTitle(R.string.noanswer_title)
                    .setMessage(R.string.noanswer_message)
                    .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                        }

                    })
                    .show();
            return;
        }

        if (viewPager.getCurrentItem() == WizardManager.WIZARD_STEP_COUNT - 1) {
            createNewGame();
            Intent intent = new Intent(this, SetActivity.class);
            startActivity(intent);
        } else {
            viewPager.setCurrentItem(viewPager.getCurrentItem() + 1);
        }
    }

    public void close(View view) {
        finish();
    }

    private void createNewGame() {
        gameManager.setCurrentGame(wizardManager.getBuilder().build());
        gameManager.getCurrentGame().set();
    }
}
