package grebic.christopher.android.poptime.game.logic;

/**
 * A guess is the entity that a participant have to describe to his peer(s).
 *
 * @author Christopher Grébic
 */
public class Guess {

    private String value;

    /**
     * Creates a new guess from a string value.
     *
     * @param value the value of the guess
     */
    public Guess(String value) {
        this.value = value;
    }

    /**
     * @return the string value of the guess
     */
    @Override
    public String toString() {
        return value;
    }
}
