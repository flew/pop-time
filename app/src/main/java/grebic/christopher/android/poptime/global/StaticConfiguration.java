package grebic.christopher.android.poptime.global;

import java.util.Arrays;
import java.util.List;

import grebic.christopher.android.poptime.game.logic.Guess;

/**
 * Static configuration of the application.
 *
 * @author Christopher Grébic
 */
public class StaticConfiguration {
    /**
     * Tag of the application
     */
    public static final String APP = "POPTIME";

    /**
     * Number of sets played during a game
     */
    public static final int SET_COUNT = 3;

    /**
     * Number of guesses for a game
     */
    public static final int GUESS_COUNT = 20;

    /**
     * Time of a turn in seconds
     */
    public static final int TURN_TIME = 40;

    /**
     * Minimum number of participants
     */
    public static final int MIN_PARTICIPANTS = 2;

    /**
     * Maximum number of participants
     */
    public static final int MAX_PARTICIPANTS = 10;

    /**
     * Default list of guesses
     */
    public static final List<Guess> defaultGuesses = Arrays.asList(
            new Guess("Michael Jackson"),
            new Guess("Donald Trump"),
            new Guess("Cersei Lannister"),
            new Guess("Batman"),
            new Guess("Jessica Alba"),
            new Guess("Larry David"),
            new Guess("Lady Gaga"),
            new Guess("Georges Clooney"),
            new Guess("Thor"),
            new Guess("Jay-Z"),
            new Guess("Adam Sandler"),
            new Guess("Stephen Hawking"),
            new Guess("Eddie Murphy"),
            new Guess("Bob Dylan"),
            new Guess("Johny Deep"),
            new Guess("Kristen Stewart"),
            new Guess("Mel Gibson"),
            new Guess("Georges Lucas"),
            new Guess("Ben Affleck"),
            new Guess("Steven Spielberg"),
            new Guess("Christopher Nolan"),
            new Guess("Bob Marley"),
            new Guess("Bon Jovi"),
            new Guess("Tommy Lee Jones"),
            new Guess("Matt Damon"),
            new Guess("Dave Grohl"),
            new Guess("Elvis Presley"),
            new Guess("Keanu Reeves"),
            new Guess("Shia LaBeouf"),
            new Guess("Bruce Lee"),
            new Guess("Teri Hatcher"),
            new Guess("Morgan Freeman"),
            new Guess("Kurt Cobain"),
            new Guess("Anne Frank"),
            new Guess("Kate Middleton"),
            new Guess("Clint Eastwood"),
            new Guess("Audrey Hepburn"),
            new Guess("Winston Churchill"),
            new Guess("Taylor Swift"),
            new Guess("Justin Trudeau"),
            new Guess("Drake"),
            new Guess("William Shatner"),
            new Guess("Fidel Castro"),
            new Guess("Ryan Reynolds"),
            new Guess("Owen Nilson"),
            new Guess("Mark Zuckerberg"),
            new Guess("Voltaire"),
            new Guess("Ariana Grande"),
            new Guess("Robert Downey Jr"),
            new Guess("Charles Darwin"),
            new Guess("Peter Jackson"),
            new Guess("Coco Chanel"),
            new Guess("Ernest Hemingway"),
            new Guess("Freddie Mercury"),
            new Guess("Dustin Hoffman"),
            new Guess("Mahatma Gandhi"),
            new Guess("Albert Einstein"),
            new Guess("Elton John"),
            new Guess("Steve Jobs"),
            new Guess("Sigmund Freud"),
            new Guess("Daniel Radcliffe"),
            new Guess("David Copperfield"),
            new Guess("James Cameron"),
            new Guess("Bruce Springsteen"),
            new Guess("Mike Tyson"),
            new Guess("Neil Armstrong"),
            new Guess("Sigourney Weaver"),
            new Guess("Justin Timberlake"),
            new Guess("Jim Parsons"),
            new Guess("50 Cent"),
            new Guess("Christian Bale"),
            new Guess("Socrate"),
            new Guess("Sean Connery"),
            new Guess("Thomas Edison"),
            new Guess("Mark Twain"),
            new Guess("Charly Sheen"),
            new Guess("George Washington"),
            new Guess("Vince Lombardi"),
            new Guess("Gwyneth Paltrow"),
            new Guess("Marilyn Monroe"),
            new Guess("Rafael Nadal"),
            new Guess("Selena Gomez"),
            new Guess("Oscar Wilde"),
            new Guess("Victoria Beckham"),
            new Guess("Ronald Reagan"),
            new Guess("Edgar Allan Poe"),
            new Guess("Walt Disney"),
            new Guess("William Shakespeare"),
            new Guess("Dr. Dre"),
            new Guess("Jackie Chan"),
            new Guess("Jeff Bezos"),
            new Guess("Joseph Stalin"),
            new Guess("Che Guevara"),
            new Guess("Ian McKellen"),
            new Guess("Zac Effron"),
            new Guess("Tom Hardy"),
            new Guess("Emma Stone"),
            new Guess("Bill Gates"),
            new Guess("Viola Davis"),
            new Guess("Dwayne Wade"),
            new Guess("Nikola Tesla"),
            new Guess("Marie Antoinette"),
            new Guess("Hulk Hogan"),
            new Guess("John Wayne"),
            new Guess("Steve Carell"),
            new Guess("Anne Hathaway"),
            new Guess("Michael Jordan"),
            new Guess("Kanye West"),
            new Guess("Isaac Newton"),
            new Guess("Tiger Woods"),
            new Guess("Pablo Picasso"),
            new Guess("Roger Federer"),
            new Guess("Channing Tantum"),
            new Guess("Paul McCartney"),
            new Guess("Jennifer Love Hewitt"),
            new Guess("Jack Nicholson")
    );
}
