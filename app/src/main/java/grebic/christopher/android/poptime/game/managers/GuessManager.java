package grebic.christopher.android.poptime.game.managers;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import grebic.christopher.android.poptime.game.logic.Guess;
import grebic.christopher.android.poptime.game.logic.Result;

/**
 * The guess manager handles the guesses throughout a game.
 *
 * @author Christopher Grébic
 */
public class GuessManager {

    private List<Guess> guesses;
    private List<Guess> availableGuesses;

    /**
     * Instantiates a new guess manager.
     * <p>
     * The provided list is copied and shuffled with {@link GuessManager#shuffle()}.
     *
     * @param guesses the list of guesses
     */
    public GuessManager(List<Guess> guesses) {
        this.guesses = guesses;
        shuffle();
    }

    /**
     * @return the available guesses
     */
    public List<Guess> getAvailableGuesses() {
        return availableGuesses;
    }

    /**
     * Shuffles the guesses.
     * <p>
     * The available guesses are reset from the original list and shuffled.
     */
    public void shuffle() {
        availableGuesses = new LinkedList<>(guesses);
        Collections.shuffle(availableGuesses);
    }

    /**
     * Merges a list of result with the available guesses.
     * <p>
     * Each negative results are reinserted in the available guesses list.
     *
     * @param results the results
     */
    public void mergeResults(List<Result> results) {
        for (Result result : results) {
            if (!result.isPositive()) {
                availableGuesses.add(result.getGuess());
            }
        }
    }
}
