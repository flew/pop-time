package grebic.christopher.android.poptime.game.managers;


import android.support.v4.app.Fragment;
import android.util.Log;

import grebic.christopher.android.poptime.game.logic.GameBuilder;
import grebic.christopher.android.poptime.global.StaticConfiguration;
import grebic.christopher.android.poptime.ui.fragments.GameModeFragment;
import grebic.christopher.android.poptime.ui.fragments.ParticipantFragment;

/**
 * The wizard manager handles the creation of a new game.
 * <p>
 * It manages the steps the user go through in order to start a new game.
 * That is selecting a game mode and entering the number of teams/players.
 *
 * @author Christopher Grébic
 * @see grebic.christopher.android.poptime.ui.fragments.GameModeFragment
 * @see grebic.christopher.android.poptime.ui.fragments.ParticipantFragment
 */
public class WizardManager {
    /**
     * The number of steps
     */
    public static final int WIZARD_STEP_COUNT = 2;

    private GameBuilder builder;

    /**
     * Instantiates a new wizard manager.
     *
     * @param builder the game builder that is use to create the game
     */
    public WizardManager(GameBuilder builder) {
        this.builder = builder;
    }

    /**
     * @return the game builder
     */
    public GameBuilder getBuilder() {
        return builder;
    }

    /**
     * Returns the fragment of the corresponding step.
     *
     * @param step the step
     * @return the fragment
     */
    public Fragment getStepFragment(int step) {
        switch (step) {
            case 0:
                return GameModeFragment.make(builder);

            case 1:
                return ParticipantFragment.make(builder);

            default:
                Log.wtf(StaticConfiguration.APP, "Invalid step in the wizard manager: " + step);
                throw new IllegalArgumentException();
        }
    }

}
